import React from "react";
import { processColor, StyleSheet, Text, View } from "react-native";
import { HorizontalBarChart } from "react-native-charts-wrapper";
import LegendItem from "./LegendItem";

const HO_ESTIMATE = processColor("#DB588A");
const HO_ACTUAL = processColor("#825F9F");
const HO_VARIANCE = processColor("#867679");
const XAXIS_LABEL_COLOR = processColor("#666666");
const VALUE_TEXT_SIZE = 12;

export default class ReportChartDetails extends React.Component {
  constructor(props) {
    super(props);
    let chartData = this.parseData(props.data);
    this.state = {
      data: {
        dataSets: [
          {
            values: chartData.hoe,
            label: "HO Estimate",
            config: {
              drawValues: true,
              colors: [HO_ESTIMATE],
              valueTextSize: VALUE_TEXT_SIZE
            }
          },
          {
            values: chartData.hoa,
            label: "HO Actual",
            config: {
              drawValues: true,
              colors: [HO_ACTUAL],
              valueTextSize: VALUE_TEXT_SIZE
            }
          },
          {
            values: chartData.hov,
            label: "HO Variance",
            config: {
              drawValues: true,
              colors: [HO_VARIANCE],
              valueTextSize: VALUE_TEXT_SIZE
            }
          }
        ],
        config: {
          barWidth: 0.2,
          group: {
            fromX: 0,
            groupSpace: 0.2,
            barSpace: 0.07
          }
        }
      },
      xAxis: {
        valueFormatter: chartData.values,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: 3,
        axisMinimum: 0,
        drawGridLines: false,
        centerAxisLabels: true,
        position: "BOTTOM",
        textSize: 14,
        textColor: XAXIS_LABEL_COLOR
      },
      yAxis: {
        left: {
          enabled: false
        },
        right: {
          drawLabels: true,
          drawAxisLine: true,
          drawGridLines: true,
          textSize: 14,
          zeroLine: {
            enabled: true,
            lineWidth: 2
          },
          enabled: true
        }
      }
    };
  }

  parseData = data => {
    let chartData = {};
    chartData.hoe = [];
    chartData.hoa = [];
    chartData.hov = [];
    chartData.values = [];
    let listOrderItems = data["OrderReport"]["OrderItem"];
    listOrderItems.forEach(element => {
      chartData.hov.push(parseInt(element.QtyDifference)); //HO Estimated
      chartData.hoa.push(parseInt(element.QtyActual)); //HO Actual
      chartData.hoe.push(parseInt(element.QtyOrdered)); //HO Variance
      chartData.values.push(element.Code);
    });
    return chartData;
  };

  handleSelect(event) {}

  render() {
    return (
      <View style={styles.footerStyle}>
        <View style={styles.chartRow}>
          <View style={styles.legendBox}>
            <LegendItem key="hoe" label="HO Estimate" itemcolor="#DB588A" />
            <LegendItem key="hoa" label="HO Actual" itemcolor="#825F9F" />
            <LegendItem key="hov" label="HO Variance" itemcolor="#867679" />
          </View>
          <View style={styles.chartContainer}>
            <View style={styles.yAxisLabelContainer}>
              <Text style={styles.yAxisLabelStyle}>{`Quantity (Crates)`}</Text>
            </View>
            <View style={styles.chartBox}>
              <HorizontalBarChart
                style={styles.chart}
                doubleTapToZoomEnabled={false}
                pinchZoom={false}
                touchEnabled={true}
                legend={this.state.legend}
                data={this.state.data}
                visibleRange={{ x: { min: 0, max: 3 } }}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                chartDescription={{ text: "" }}
                legend={{ enabled: false }}
                onSelect={this.handleSelect.bind(this)}
                onChange={event => console.log(event.nativeEvent)}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  legendBox: {
    height: 40,
    width: "100%",
    flexDirection: `row`,
    justifyContent: "center"
  },
  yAxisLabelStyle: {
    textAlign: `center`,
    height: 20,
    width: 200
  },
  yAxisLabelContainer: {
    paddingTop: 10,
    width: `100%`,
    height: `10%`,
    alignItems: `center`,
    justifyContent: "center"
  },
  chartBox: {
    width: `90%`,
    height: `100%`
  },
  chartContainer: {
    width: `100%`,
    height: "80%",
    alignItems: `center`,
    justifyContent: `center`,
    flexDirection: `column`
  },
  chartRow: {
    width: `100%`,
    height: "100%"
  },
  footerStyle: {
    width: `100%`,
    height: `100%`,
    flexDirection: `column`,
    alignItems: `center`,
    justifyContent: `center`
  },
  chart: {
    width: "100%",
    height: "100%"
  }
});
