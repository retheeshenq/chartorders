import React from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  processColor
} from "react-native";

import { BarChart } from "react-native-charts-wrapper";
import LegendItem from "./LegendItem";

const HO_ESTIMATE = processColor("#DB588A");
const HO_ACTUAL = processColor("#825F9F");
const HO_VARIANCE = processColor("#867679");

export default class ReportChart extends React.Component {
  constructor(props) {
    super(props);
    let chartData = this.parseData(props.data);
    this.state = {
      data: {
        dataSets: [
          {
            values: [
              { y: chartData.values[0] },
              { y: chartData.values[1] },
              { y: chartData.values[2] }
            ],
            label:"RanchName",
            config: {
              valueTextSize: 12,
              colors: [HO_ESTIMATE, HO_ACTUAL, HO_VARIANCE]
            }
          }
        ],
        config: {
          barWidth: 0.5
        }
      },
      xAxis: {
        enabled: false
      },
      yAxis: {
        left: {
          drawLabels: true,
          drawAxisLine: true,
          drawGridLines: true,
          zeroLine: {
            enabled: true,
            lineWidth: 2
          }
        },
        right: {
          enabled: false
        }
      }
    };
  }

  handleSelect(event) {}

  parseData = data => {
    let chartData = {};
    chartData.values = [];
    let element = data["OrderReport"];
    chartData.values.push(parseInt(element.QtyOrderedTotal)); //HO Estimated
    chartData.values.push(parseInt(element.QtyActualTotal)); //HO Actual
    chartData.values.push(parseInt(element.QtyTotalDifference)); //HO Variance
    return chartData;
  };

  render() {
    return (
      <View style={styles.footerStyle}>
        <View style={styles.chartRow}>
          <View style={styles.legendBox}>
            <LegendItem key="hoe" label="HO Estimate" itemcolor="#DB588A" />
            <LegendItem key="hoa" label="HO Actual" itemcolor="#825F9F" />
            <LegendItem key="hov" label="HO Variance" itemcolor="#867679" />
          </View>
          <View style={styles.chartContainer}>
            <View style={styles.yAxisLabelContainer}>
              <Text style={styles.yAxisLabelStyle}>{`Quantity (Crates)`}</Text>
            </View>
            <View style={styles.chartBox}>
              <BarChart
                style={styles.chart}
                doubleTapToZoomEnabled={false}
                pinchZoom={false}
                touchEnabled={true}
                legend={this.state.legend}
                data={this.state.data}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                chartDescription={{ text: "" }}
                legend={{ enabled: false }}
                onSelect={this.handleSelect.bind(this)}
                onChange={event => console.log(event.nativeEvent)}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  legendBox: {
    height: 40,
    width: "100%",
    flexDirection: `row`,
    justifyContent: "center"
  },
  yAxisLabelStyle: {
    transform: [{ rotate: "90deg" }],
    position: `relative`,
    marginTop: 100,
    textAlign: `center`,
    width: 200
  },
  yAxisLabelContainer: {
    width: `10%`,
    height: `100%`,
    alignItems: `center`
  },
  chartBox: {
    width: `90%`,
    height: `100%`
  },
  chartContainer: {
    width: `100%`,
    height: 200,
    alignItems: `center`,
    justifyContent: `center`,
    flexDirection: `row`
  },
  chartRow: {
    width: `100%`,
    height: 200
  },
  footerStyle: {
    width: `100%`,
    height: `100%`,
    flexDirection: `column`,
    alignItems: `center`,
    justifyContent: `center`
  },
  chart: {
    width: "100%",
    height: 200
  }
});
