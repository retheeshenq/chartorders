import React, { Fragment } from "react";
import { View, Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  legendBoxStyle: {
    height: `100%`,
    width: 20,
    backgroundColor: `rgba(19, 19, 223, 1)`,
    marginRight: 10
  },
  legendItemStyle: {
    width:108,
    height: 20,
    flexDirection: `row`,
    paddingLeft: 1,
    alignItems: `center`,
    justifyContent: `center`
  }
});

class LegendItem extends React.PureComponent {
  render() {
    return (
      <Fragment>
        <View style={styles.legendItemStyle}>
          <View
            style={[
              styles.legendBoxStyle,
              { backgroundColor: this.props.itemcolor }
            ]}
          />
          <Text>{this.props.label}</Text>
        </View>
      </Fragment>
    );
  }
}

LegendItem.defaultProps = {};

export default LegendItem;

export { styles };
